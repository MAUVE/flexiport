#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <flexiport/flexiport.h>
#include <flexiport/port.h>

int main(void)
{
  flexiport::Port *port;
  std::cout << "[TCP Server]: Create port" << std::endl;

  try
  {
    port = flexiport::CreatePort ("type=tcp,listen,port=21500");
    std::cout << port->GetStatus ();
    std::cout << "[TCP Server]: Opening port and wait connection" << std::endl;
    port->Open();
    port->SetTimeout(flexiport::Timeout(1,0));
  }
  catch(flexiport::PortException e)
  {
    std::cerr << "Caught exception: " << e.what () << std::endl;
    return 1;
  }

  while(1)
  {
    if(port->IsOpen())
    {
      std::string response_msg;
      port->ReadString(response_msg);
      std::cout << "Receive msg: " << response_msg << std::endl;
      if((response_msg=="hello")&&(port->IsOpen()))
         port->WriteString("ACK");
    }
    else
    {
      std::cout << "[TCP Server]: Port has been closed" << std::endl;
      sleep(1);
      std::cout << "[TCP Server]: Restart server" << std::endl;
      port->Open();
    }
  }
  return 0;
}
