#include <string.h>
#include <sys/types.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <flexiport/flexiport.h>
#include <flexiport/port.h>

int main(void)
{
  flexiport::Port *port;
  std::cout << "[TCP Client]: Create port" << std::endl;
  try
  {
    port = flexiport::CreatePort ("type=tcp,ip=127.0.0.1,port=21500,timeout=0");//.001");
    std::cout << port->GetStatus ();
  }
  catch(flexiport::PortException e)
  {
    std::cerr << "Caught exception: " << e.what () << std::endl;
    return 1;
  }


  while(1)
  {
    if(!port->IsOpen())
    {
      std::cout << "[TCP Client]: Try to connect port" << std::endl;
      try
      {
        port->Open();
      }
      catch(flexiport::PortException e)
      {
        std::cerr << "Open exception: " << e.what () << std::endl;
      }
    }
    else
    {
      std::cout << "[TCP Client]: Send message=hello" << std::endl;
      try
      {
        port->WriteString("hello");
      }
      catch(flexiport::PortException e)
      {
        std::cerr << "Write exception: " << e.what () << std::endl;
      }
      std::string response_msg;
      try
      {
        if(port->ReadString(response_msg)!=-1)
          std::cout << "[TCP Client]: Receive msg="
            << response_msg << std::endl;
      }
      catch(flexiport::PortException e)
      {
        std::cerr << "Read exception: " << e.what () << std::endl;
      }
    }
    sleep(1);
  }

  return 0;
}
